package in.dhuma1981.androidtask;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import in.dhuma1981.androidtask.bean.Node;
import in.dhuma1981.androidtask.custom.NodeView;


public class MainActivity extends ActionBarActivity implements View.OnClickListener {

    private List<String> citynames;
    private List<String> citycodes;
    private ArrayAdapter<String> adapter;

    private HorizontalScrollView hsvMain;
    private Button btnAddView;
    private LinearLayout llContainer;
    private LayoutInflater inflater;
    private int i=0;
    private View view;
    private NodeView mynode;
    private Spinner spnCity;

    private ArrayList<Node> nodes;
    private Node selectedNode=new Node();
    private NodeView myNodeView;
    private CheckBox cbAccommodation,cbTaxi;

    private TextView tvDate;

    private DatePickerDialog datePickerDialog;
    private ImageButton ibtnRemove;

    private int selectedIndex;
    private int lastSelectedIndex=-1;

    Calendar newCalendar;
    private int mYear,mMonth,mDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnAddView=(Button)findViewById(R.id.btn_add_node);
        llContainer=(LinearLayout)findViewById(R.id.ll_container_main);
        spnCity=(Spinner)findViewById(R.id.spn_city);
        cbAccommodation=(CheckBox)findViewById(R.id.cb_accommodation);
        cbTaxi=(CheckBox)findViewById(R.id.cb_taxi);
        tvDate=(TextView)findViewById(R.id.tv_date);
        hsvMain=(HorizontalScrollView)findViewById(R.id.hsv_main);

        citynames= Arrays.asList(getResources().getStringArray(R.array.city));
        citycodes= Arrays.asList(getResources().getStringArray(R.array.city_code));
        adapter=new ArrayAdapter<>(MainActivity.this,android.R.layout.simple_list_item_single_choice,citynames);
        spnCity.setAdapter(adapter);

        inflater=LayoutInflater.from(MainActivity.this);
        nodes=new ArrayList<>();

        newCalendar=Calendar.getInstance();
        mYear= newCalendar.get(Calendar.YEAR);
        mMonth= newCalendar.get(Calendar.MONTH);
        mDate=newCalendar.get(Calendar.DAY_OF_MONTH);

        for(;i<3;i++){
            Node node=new Node();
            view=inflater.inflate(R.layout.node_view,null);
            mynode=(NodeView)view.findViewById(R.id.nv_node_view);
            mynode.setDate(mYear,mMonth,mDate);
            mynode.setNode(node);
            ibtnRemove=(ImageButton)mynode.findViewById(R.id.ibtn_remove);
            view.setTag(i);
            view.setOnClickListener(MainActivity.this);
            ibtnRemove.setOnClickListener(MainActivity.this);
            if(i==0){
                mynode.firstNode();
            }
            mynode.defaultNode();
            nodes.add(node);
            llContainer.addView(view);
        }

        btnAddView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Node node = new Node();
                nodes.add(node);
                view = inflater.inflate(R.layout.node_view, null);
                mynode = (NodeView) view.findViewById(R.id.nv_node_view);
                ibtnRemove=(ImageButton)mynode.findViewById(R.id.ibtn_remove);
                view.setTag(i);
                view.setOnClickListener(MainActivity.this);
                ibtnRemove.setOnClickListener(MainActivity.this);
                llContainer.addView(view);
                i++;
                hsvMain.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        hsvMain.fullScroll(ScrollView.FOCUS_RIGHT);
                    }
                }, 100L);
            }
        });

        cbAccommodation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(myNodeView!=null) {
                    if (isChecked) {
                        myNodeView.setAccommodation();
                    } else {
                        myNodeView.resetAccommodation();
                    }
                }
            }
        });

        cbTaxi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(myNodeView!=null) {
                    if (isChecked) {
                        myNodeView.setTaxi();
                    } else {
                        myNodeView.resetTaxi();
                    }
                }
            }
        });

        spnCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(myNodeView!=null){
                    String cityName=spnCity.getSelectedItem().toString();
                    cityName=cityName.substring(0,(cityName.indexOf("-")-1));
                    myNodeView.setCityName(cityName);
                    myNodeView.setCityCode(citycodes.get(position));
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });





        datePickerDialog=new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                if(myNodeView!=null)
                    myNodeView.setDate(year,monthOfYear,dayOfMonth);
            }
        },mYear, mMonth, mDate){
            @Override
            public void onDateChanged(DatePicker view, int year, int month, int day) {
                if (year < mYear)
                    view.updateDate(mYear, mMonth, mDate);

                if (month < mMonth && year == mYear)
                    view.updateDate(mYear, mMonth, mDate);

                if (day < mDate && year == mYear
                        && month == mMonth)
                    view.updateDate(mYear, mMonth, mDate);
            }
        };
        tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show();
            }
        });
    }


    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.ibtn_remove){
            if(i>3) {
                llContainer.removeViewAt(--i);
                hsvMain.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        hsvMain.fullScroll(ScrollView.FOCUS_RIGHT);
                    }
                }, 100L);
            }
        }else {
            selectedIndex = Integer.parseInt(v.getTag().toString());
            if(selectedIndex<i) {
                selectedNode = nodes.get(selectedIndex);
                myNodeView = (NodeView) llContainer.getChildAt(selectedIndex).findViewById(R.id.nv_node_view);
                myNodeView.activeNode();

                Node node=myNodeView.getNode();
                if(node.isAccommodation()){
                    cbAccommodation.setChecked(true);
                }else{
                    cbAccommodation.setChecked(false);
                }
                if(node.isTaxi()){
                    cbTaxi.setChecked(true);
                }else{
                    cbTaxi.setChecked(false);
                }

                if (lastSelectedIndex != selectedIndex && lastSelectedIndex != -1 && lastSelectedIndex<i) {
                    NodeView lastSelectedNode = (NodeView) llContainer.getChildAt(lastSelectedIndex).findViewById(R.id.nv_node_view);
                    lastSelectedNode.processedNode();
                }
                lastSelectedIndex = selectedIndex;
                if((selectedIndex-1)>-1) {
                    NodeView previousNode = (NodeView) llContainer.getChildAt(selectedIndex - 1).findViewById(R.id.nv_node_view);
                    mYear=previousNode.getYear();
                    mMonth=previousNode.getMonth();
                    mDate=previousNode.getDate();
                }else{
                    mYear= newCalendar.get(Calendar.YEAR);
                    mMonth= newCalendar.get(Calendar.MONTH);
                    mDate=newCalendar.get(Calendar.DAY_OF_MONTH);
                }
            }
        }
    }
}
