package in.dhuma1981.androidtask.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Calendar;
import java.util.GregorianCalendar;

import in.dhuma1981.androidtask.R;
import in.dhuma1981.androidtask.bean.Node;

public class NodeView extends RelativeLayout {

    private Context context;
    private Node node;
    private TextView tvCityCode,tvCityName,tvDate,tvMonthYear,tvDay;
    private ImageView ivAccomodation,ivTaxi,ivArrow;
    private ImageView ivLeftLine,ivRightLine,ivNode;
    private ImageButton ibtnRemoveNode;
    private RelativeLayout rlDate,rlRemove;
    private int mYear,mMonth,mDate;

    public NodeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context=context;

        View view=LayoutInflater.from(context).inflate(R.layout.single_node,this);
        tvCityCode=(TextView)view.findViewById(R.id.tv_city_code_single_node);
        tvCityName=(TextView)view.findViewById(R.id.tv_city_name_single_node);
        tvDate=(TextView)view.findViewById(R.id.tv_date_single_node);
        tvMonthYear=(TextView)view.findViewById(R.id.tv_month_year_single_node);
        tvDay=(TextView)view.findViewById(R.id.tv_day_single_node);
        ivAccomodation=(ImageView)view.findViewById(R.id.iv_accomodation_single_node);
        ivNode=(ImageView)view.findViewById(R.id.iv_node_single_node);
        ivTaxi=(ImageView)view.findViewById(R.id.iv_taxi_single_node);
        ivLeftLine=(ImageView)view.findViewById(R.id.iv_left_line);
        ivArrow=(ImageView)view.findViewById(R.id.iv_arrow);
        ivRightLine=(ImageView)view.findViewById(R.id.iv_right_line);
        ibtnRemoveNode=(ImageButton)view.findViewById(R.id.ibtn_remove);
        rlDate=(RelativeLayout)view.findViewById(R.id.rl_date);
        rlRemove=(RelativeLayout)view.findViewById(R.id.rl_remove_node);
        node=new Node();

    }

    public void setNode(Node node){
        this.node=node;
    }

    public Node getNode(){
        return node;
    }

    public void setCityName(String cityName){
        node.setCity_name(cityName);
        tvCityName.setText(cityName);
    }

    public void setCityCode(String cityCode){
        node.setCity_code(cityCode);
        tvCityCode.setText(cityCode);
    }

    public void setAccommodation(){
        node.setAccommodation(true);
        ivAccomodation.setVisibility(ImageView.VISIBLE);
    }

    public void resetAccommodation(){
        node.setAccommodation(false);
        ivAccomodation.setVisibility(ImageView.INVISIBLE);
    }

    public void setTaxi(){
        node.setTaxi(true);
        ivTaxi.setVisibility(ImageView.VISIBLE);
    }
    public void resetTaxi(){
        node.setTaxi(false);
        ivTaxi.setVisibility(ImageView.INVISIBLE);
    }

    public void setDate(int year,int month,int date){
        mYear=year;
        mMonth=month;
        mDate=date;
        rlRemove.setVisibility(View.GONE);
        rlDate.setVisibility(View.VISIBLE);
        String day=getDayFromDate(mYear,mMonth,mDate);
        String monthName=getMonthString(mMonth);
        if(!day.equals("")){
            String strYear=mYear+"";
            tvMonthYear.setText(monthName+" "+strYear.substring(2));
            tvDay.setText(day);
            tvDate.setText(mDate+"");
            node.setMonth_year(monthName+" "+strYear.substring(2));
            node.setDay(day);
            node.setDate(mDate+"");
        }
    }

    public String getDayFromDate(int year,int month,int date){
        Calendar calendar=new GregorianCalendar(year,month,date);
        int result=calendar.get(Calendar.DAY_OF_WEEK);
        switch (result)
        {
            case Calendar.SUNDAY:
                return "Sun";

            case Calendar.MONDAY:
                return "Mon";

            case Calendar.TUESDAY:
                return "Tue";

            case Calendar.WEDNESDAY:
                return "Wed";

            case Calendar.THURSDAY:
                return "Thu";

            case Calendar.FRIDAY:
                return "Fri";

            case Calendar.SATURDAY:
                return "Sat";
            default:
                return "";
        }
    }

    public String getMonthString(int month){
        switch (month){
            case 0:
                return "Jan";
            case 1:
                return "Feb";
            case 2:
                return "Mar";
            case 3:
                return "Apr";
            case 4:
                return "May";
            case 5:
                return "Jun";
            case 6:
                return "Jul";
            case 7:
                return "Aug";
            case 8:
                return "Sep";
            case 9:
                return "Oct";
            case 10:
                return "Nov";
            case 11:
                return "Dec";
            default:
                return "";
        }
    }

    public void firstNode(){
        ivLeftLine.setVisibility(View.GONE);
        ivArrow.setVisibility(View.GONE);
    }

    public void defaultNode(){
        rlDate.setVisibility(VISIBLE);
        rlRemove.setVisibility(GONE);
    }

    public void activeNode(){
        ivNode.setBackgroundResource(R.drawable.circle_yellow);
    }

    public void processedNode(){
        ivNode.setBackgroundResource(R.drawable.circle_green);
    }

    public int getYear(){ return  mYear; }
    public int getMonth(){ return  mMonth; }
    public int getDate(){ return  mDate; }

}
