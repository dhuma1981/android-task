package in.dhuma1981.androidtask.bean;

public class Node {


    public void setCity_code(String city_code) {
        this.city_code = city_code;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setMonth_year(String month_year) {
        this.month_year = month_year;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public boolean isAccommodation() {
        return accommodation;
    }

    public void setAccommodation(boolean accommodation) {
        this.accommodation = accommodation;
    }

    public boolean isTaxi() {
        return taxi;
    }

    public void setTaxi(boolean taxi) {
        this.taxi = taxi;
    }

    private String city_code,city_name,date,month_year,day;
    private boolean accommodation,taxi;
}
